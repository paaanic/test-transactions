CREATE TABLE IF NOT EXISTS users
(
    id INTEGER NOT NULL PRIMARY KEY,
    balance double precision
);

INSERT INTO public.users (id, balance) VALUES (1, 1000);
INSERT INTO public.users (id, balance) VALUES (2, 2000);
INSERT INTO public.users (id, balance) VALUES (3, 3000);
