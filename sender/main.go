package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"

	validator "github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/spf13/viper"
)

type Config struct {
	RmqURL string
}

type Transaction struct {
	UserID int     `validate:"required" json:"userID"`
	Amount float64 `validate:"required,numeric" json:"amount"`
}

func getConfig() Config {
	viper.AutomaticEnv()

	return Config{RmqURL: viper.GetString("rabbit_url")}
}

type ErrorResponse struct {
	FailedField string
	Tag         string
	Value       string
}

var validate = validator.New()

func validateTr(tr Transaction) []*ErrorResponse {
	var errors []*ErrorResponse
	err := validate.Struct(tr)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ErrorResponse
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}
	return errors
}

func sendMsg(userID int, amount float64) error {
	config := getConfig()

	rmqConn, err := amqp.Dial(config.RmqURL)
	if err != nil {
		panic(err)
	}
	defer rmqConn.Close()

	rmqChan, err := rmqConn.Channel()
	if err != nil {
		panic(err)
	}
	defer rmqChan.Close()

	qName := fmt.Sprintf("%d.tr", userID)
	_, err = rmqChan.QueueDeclare(qName, true, false, false, false, nil)
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	body := []byte(strconv.FormatFloat(amount, 'f', -1, 64))
	if err := rmqChan.PublishWithContext(
		ctx,
		"",    // exchange
		qName, // queue name
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		},
	); err != nil {
		return err
	}

	return nil
}

func getMsg(userID int) (int, error) {
	config := getConfig()

	rmqConn, err := amqp.Dial(config.RmqURL)
	if err != nil {
		panic(err)
	}
	defer rmqConn.Close()

	rmqChan, err := rmqConn.Channel()
	if err != nil {
		panic(err)
	}
	defer rmqChan.Close()

	qName := fmt.Sprintf("%d.trRes", userID)
	_, err = rmqChan.QueueDeclare(qName, true, false, false, false, nil)
	if err != nil {
		panic(err)
	}

	messages, err := rmqChan.Consume(
		qName, // queue name
		"",    // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no local
		false, // no wait
		nil,   // arguments
	)
	if err != nil {
		return 0, err
	}

	for {
		select {
		case <-time.After(time.Second * 30):
			return 0, errors.New("timeout error")
		case msg := <-messages:
			status, err := strconv.Atoi(string(msg.Body))
			return status, err
		}
	}
}

func main() {
	viper.AutomaticEnv()
	rabbitURL := viper.GetString("rabbit_url")

	conn, err := amqp.Dial(rabbitURL)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	app := fiber.New()

	app.Use(
		logger.New(),
	)

	app.Post("/transactions", func(c *fiber.Ctx) error {
		tr := new(Transaction)

		if err := c.BodyParser(tr); err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"message": err.Error(),
			})

		}

		errors := validateTr(*tr)
		if errors != nil {
			return c.Status(fiber.StatusUnprocessableEntity).JSON(errors)

		}

		if err := sendMsg(tr.UserID, tr.Amount); err != nil {
			return c.SendStatus(fiber.StatusInternalServerError)
		}

		trStatus, err := getMsg(tr.UserID)
		if err != nil {
			return c.SendStatus(fiber.StatusInternalServerError)
		}

		switch trStatus {
		case 1:
			return c.Status(fiber.StatusOK).JSON(tr)
		case 2:
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"message": "Transcation declined",
			})
		}

		return c.SendStatus(fiber.StatusInternalServerError)
	})

	log.Fatal(app.Listen(":8000"))
}
