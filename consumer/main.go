package main

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"time"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/spf13/viper"
)

type Config struct {
	RmqURL           string
	PostgresHost     string
	PostgresPort     int32
	PostgresDB       string
	PostgresUser     string
	PostgresPassword string
}

func getConfig() Config {
	viper.AutomaticEnv()

	return Config{
		RmqURL:           viper.GetString("rabbit_url"),
		PostgresHost:     viper.GetString("postgres_host"),
		PostgresPort:     viper.GetInt32("postgres_port"),
		PostgresDB:       viper.GetString("postgres_db"),
		PostgresUser:     viper.GetString("postgres_user"),
		PostgresPassword: viper.GetString("postgres_password"),
	}
}

type User struct {
	ID      int
	Balance float64
}

func getUsersIDs(db *sqlx.DB) []int {
	query := "SELECT id FROM users"
	users := []int{}
	db.Select(&users, query)
	return users
}

func getBalance(db *sqlx.DB, userID int) (float64, error) {
	query := "SELECT balance FROM users WHERE id=$1"
	var balance float64
	err := db.Get(&balance, query, userID)
	return balance, err
}

func updateBalance(db *sqlx.DB, userID int, amount float64) error {
	query := `
	UPDATE users
	SET balance = balance + $2
	WHERE id=$1`

	_, err := db.Exec(query, userID, amount)
	return err
}

func handleMessage(db *sqlx.DB, userID int, msg []byte) {
	var (
		amount, balance float64
		err             error
	)

	if amount, err = strconv.ParseFloat(string(msg), 64); err != nil {
		log.Println(err)
		return
	}
	if balance, err = getBalance(db, userID); err != nil {
		log.Println(err)
		return
	}
	if balance+amount < 0 {
		log.Println("Can't process transaction")
		sendMsg(userID, 2)
		return
	}

	if err = updateBalance(db, userID, amount); err != nil {
		log.Println(err)
		sendMsg(userID, 2)
		return
	}

	sendMsg(userID, 1)
}

func sendMsg(userID int, status uint8) error {
	config := getConfig()

	rmqConn, err := amqp.Dial(config.RmqURL)
	if err != nil {
		panic(err)
	}
	defer rmqConn.Close()

	rmqChan, err := rmqConn.Channel()
	if err != nil {
		panic(err)
	}
	defer rmqChan.Close()

	qName := fmt.Sprintf("%d.trRes", userID)
	_, err = rmqChan.QueueDeclare(qName, true, false, false, false, nil)
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	body := []byte(strconv.Itoa(int(status)))
	if err := rmqChan.PublishWithContext(
		ctx,
		"",    // exchange
		qName, // queue name
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		},
	); err != nil {
		return err
	}

	return nil
}

func consumeByUser(db *sqlx.DB, userID int) {

	cfg := getConfig()

	rmqConn, err := amqp.Dial(cfg.RmqURL)
	if err != nil {
		panic(err)
	}
	defer rmqConn.Close()

	rmqChan, err := rmqConn.Channel()
	if err != nil {
		panic(err)
	}
	defer rmqChan.Close()

	qName := fmt.Sprintf("%d.tr", userID)
	_, err = rmqChan.QueueDeclare(qName, true, false, false, false, nil)
	if err != nil {
		panic(err)
	}

	messages, err := rmqChan.Consume(
		qName, // queue name
		"",    // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no local
		false, // no wait
		nil,   // arguments
	)
	if err != nil {
		log.Println(err)
	}

	for msg := range messages {
		handleMessage(db, userID, msg.Body)
	}
}

func main() {
	config := getConfig()

	db, err := sqlx.Connect("pgx",
		fmt.Sprintf(
			"host=%s port=%d dbname=%s user=%s password=%s sslmode=disable",
			config.PostgresHost, config.PostgresPort, config.PostgresDB,
			config.PostgresUser, config.PostgresPassword,
		),
	)
	if err != nil {
		log.Fatalln(err)
	}

	usersIDs := getUsersIDs(db)

	wait := make(chan bool)

	for _, userID := range usersIDs {
		go consumeByUser(db, userID)
	}

	<-wait
}
